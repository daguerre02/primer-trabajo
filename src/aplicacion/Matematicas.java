/**
 Copyright 2021 Alberto Daguerre Torres
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

package aplicacion;

/**
 * Esta clase contiene el método toString y el método "annadirMarca" y la
 * creación de un ArrayList para que se añadan las marcas a medida que el
 * usuario vaya añadiendo más.
 * 
 * @author Alberto Daguerre
 * @version final 01/03/2021
 */

public class Matematicas {
    static double aciertos = 0;
    static double areaCuadrado = 4;
    static double radio = 1;

    /**
     * Este método sirve para calcular nuestro resultado. Para ello se utiliza un
     * bucle for para hallar los aciertos de los puntos generados según el número de
     * puntos ponga el usuario.
     * 
     * @param puntosMaximosTotales es de tipo double y será el numero que introduzca el usuario.
     */

    public static double calculoEstimacionDePi(double puntosMaximosTotales) {

        for (int puntosTotales = 1; puntosTotales <= puntosMaximosTotales; puntosTotales++) {
            double x = (Math.random() * 2 - 1);
            double y = (Math.random() * 2 - 1);
            if (Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) <= 1)
                aciertos += 1;
        }

        double calculoAreaCirculo = (areaCuadrado * (aciertos / puntosMaximosTotales));

        double estimacionDePi = (calculoAreaCirculo / Math.pow(radio, 2));

        System.out.println("La aproximacion a Pi mediante Montecarlo es :" + estimacionDePi);

        return estimacionDePi;

    }
}
