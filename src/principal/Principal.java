/**
 Copyright 2021 Alberto Daguerre Torres
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

package principal;

import aplicacion.Matematicas;

/**
 * Esta clase contiene el método toString y el método "annadirMarca" y la
 * creación de un ArrayList para que se añadan las marcas a medida que el
 * usuario vaya añadiendo más.
 * 
 * @author Alberto Daguerre
 * @version final 01/03/2021
 */

public class Principal {

    /**
     * Esta es la clase main en la cual se establece un parametro para que se guarde
     * el número que quiera el usuario y qu emás tarde se imprima el resultado.
     * 
     * @param args entrada que establece el numero de datos
     */
    public static void main(String[] args) {

        double puntosMaximosTotales = Double.parseDouble(args[0]);

        Matematicas.calculoEstimacionDePi(puntosMaximosTotales);
    }
}
