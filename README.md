![copyright1.jpg](../../Primer%20trabajo1.0/primer-trabajo/src/copyright1.jpg)

/*Copyright [2021] [Alberto Daguerre Torres]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

# Participantes:

* Alberto Daguerre Torres.

# Objetivo:

* Este programa se ha creado para poder calcular un aproximación al numero Pi mediante un número cualquiera introducido por el usuario que esté dentro del rango establecido, mediante el método de Montecarlo.

# Funcionalidad:

* Para que el programa se ejecute y que calcule la estimación de Pi mediante Montecarlo, deberá escribir en el termial las siguientes instrucciones:
*   1. make jar
*   2. java -jar Montecarlo.jar numero_que_quiera_introduccir
